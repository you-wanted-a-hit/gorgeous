import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import {HomePageComponent} from './home-page'
import { SearchComponent } from './search';
import {StatisticsComponent} from'./statistics';


const routes: Routes = [
    
    { path: '', component: HomePageComponent },
    { path: 'search', component: SearchComponent },
    { path: 'statistic', component: StatisticsComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);