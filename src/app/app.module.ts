import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';
import { MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input'
import {HttpClientModule} from '@angular/common/http'
import { SearchService } from './search.service';
import { ChartModule } from 'angular2-chartjs';
import { ChartsModule } from 'ng2-charts';
import { appRoutingModule } from './app.routing';
import { HomePageComponent } from './home-page/home-page.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ResultComponent } from './result/result.component';
import { MatTableModule } from '@angular/material/table';
import { TrackComponent } from './track/track.component';
import { RecomendationComponent } from './recomendation/recomendation.component' 
import {SafePipe} from '../pipes/safePipe'

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SearchComponent,
    HomePageComponent,
    StatisticsComponent,
    ResultComponent,
    TrackComponent,
    RecomendationComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    appRoutingModule,
    MatTableModule,
    ChartModule,
    ChartsModule
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
