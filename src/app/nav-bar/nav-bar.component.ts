import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Output() public like = new EventEmitter<any>();
  @Input() numOfLikes: number;
  public url:string;
  constructor() { }

  ngOnInit(): void {
  }
  likeIt(){
    this.like.emit();
  }
  shareIt(){
    this.url=window.location.href;
    console.log(this.url);
    alert("The ")

  }

}
