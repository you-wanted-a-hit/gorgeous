import { TestBed } from '@angular/core/testing';

import { JsonToArrService } from './json-to-arr.service';

describe('JsonToArrService', () => {
  let service: JsonToArrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonToArrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
