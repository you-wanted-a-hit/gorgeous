import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { finalResult } from '../interfaces/finalResult';
import { song } from '../interfaces/song';
import { track } from '../interfaces/track-folder/track';

@Component({
  selector: 'app-recomendation',
  templateUrl: './recomendation.component.html',
  styleUrls: ['./recomendation.component.css']
})
export class RecomendationComponent implements OnChanges {
  @Input() similar_songs: track[];
  recomandationDetails:similar_song[] = [];

  constructor() { }

  ngOnInit(): void {
  }
  displayedColumns: string[] = ['Link', 'Song', 'Artist'];
  dataSource = new MatTableDataSource<similar_song>();
  nameAudioMapping = {};
  audioLink: string = "";
  pageSizeOptions:number[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  ngOnChanges(changes:SimpleChanges) {
    debugger;
    this.similar_songs = changes.similar_songs.currentValue
    
    this.dataSource.paginator = this.paginator;
    this.setData();
    this.dataSource = new MatTableDataSource<similar_song>(this.recomandationDetails);

  }
  setData(){
    debugger;
    for(let i=0;i<this.similar_songs.length;i++){
      debugger;
      var songName = this.similar_songs[i].title;
      var link = this.similar_songs[i].preview_url;
      var artist= this.similar_songs[i].artists[0].name;
      var position = i+1;
      this.nameAudioMapping[songName] = link
      this.recomandationDetails.push(new similar_song(songName, artist, link, position))
    }
    debugger;
  }
  
  onAudioClick(songName){
    var audioPlayer = document.getElementById('audio-player') as HTMLAudioElement;
    audioPlayer.pause();
    if(this.nameAudioMapping[songName] != audioPlayer.src)
    {
      audioPlayer.src = this.nameAudioMapping[songName];
      audioPlayer.play()
    }
  }

}
export class similar_song {
  Song: string;
  position: number;
  Artist: string;
  Link: string;

  constructor(songName, artist, link, position) {
    this.Song = songName;
    this.Artist = artist;
    this.Link= link;
    this.position = position;
  }
}

