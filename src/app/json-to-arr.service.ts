import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//import clustering_res from '../assets/clustering_res.json';


@Injectable({ 
  providedIn: 'root'
})
export class JsonToArrService {

  constructor(private http: HttpClient) { }

  public getSongsElement(): Observable<SongElement[]> {
    
    console.log('the json data is'+ this.http.get<SongElement[]>('../assets/clustering_res.json'))
    return this.http.get<SongElement[]>('../assets/clustering_res.json');
    
  }
}

export class SongElement{
  spotify_id: any;
  Position: any;
  Artist: any;
  TrackName: any;
  Streams:any;
  instrumentalness:any;
  energy:any;
  loudness:any;
  speechiness:any;
  acousticness:any;
  liveness:any;
  valence:any;
  tempo:any;
  cluster:any;
  dist:any;

  constructor(obj: any) {
    this.spotify_id =  obj.spotify_id;
    this.Position=obj.Position;
    this.Artist= obj.Position;
    this.TrackName= obj.Position;
    this.Streams=obj.Position;
    this.instrumentalness=obj.Position;
    this.energy=obj.Position;
    this.loudness=obj.Position;
    this.speechiness=obj.Position;
    this.acousticness=obj.Position;
    this.liveness=obj.Position;
    this.valence=obj.Position;
    this.tempo=obj.Position;
    this.cluster=obj.Position;
    this.dist=obj.Position;
  }
}

// export interface SongElement {
//   spotify_id: any;
//   Position: any;
//   Artist: any;
//   Track Name: any;
//   Streams:any;
//   instrumentalness:any;
//   energy:any;
//   loudness:any;
//   speechiness:any;
//   acousticness:any;
//   liveness:any;
//   valence:any;
//   tempo:any;
//   cluster:any;
//   dist:any;

// }