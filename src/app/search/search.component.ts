import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import  track  from '../../assets/track.json'


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Input() myTrack: any[];
  @Output() public search = new EventEmitter<any>();
  // @Output() public show = new EventEmitter<any>();
  public songRec: FormGroup;
  public buttonDisabled: boolean; 
  
  constructor() { 
    this.buildForm()
  }
  
  buildForm(){
    this.songRec=new FormGroup
    ({
        songName    : new FormControl('',Validators.required),
        artistName  : new FormControl('',Validators.required)
      }
    )
  }

  onSearch(){
    debugger;
    if (this.songRec.valid){
      this.buttonDisabled = true;
      this.search.emit(this.songRec.value);
      this.sleep(6000)
      // this.buttonDisabled = false;
      console.log(this.songRec.value)
    }
    else{
      alert('Please put all required fields')
    }
  }

  get songName(): any {
    return this.songRec.get('songName');
  }

  get artistName() :any {
    return this.songRec.get('artistName');
  }
  // onShow(){
  //   this.show.emit();

  // }
  
  // Print() : void {
  //   debugger;
  //   console.log(this.myTrack);
  // }

  ngOnInit() {

  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}