import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core'
// import  track  from '../assets/track.json'
import { SearchService } from './search.service';
import result from '../assets/result.json';
import { HttpClient } from '@angular/common/http';
import { finalResult } from './interfaces/finalResult';
import { Color, Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType, RadialChartOptions } from 'chart.js';
import { AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { songsTable } from './interfaces/songsTable';
import { track } from './interfaces/track-folder/track';
import { classificationInsights } from './interfaces/insight-folder/classificationInsights';
import { DomSanitizer } from '@angular/platform-browser';

const months = Array.from({ length: 12 }, (item, i) => {
  return new Date(0, i).toLocaleString('en-US', { month: 'long' })
});

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  public isThereRes: boolean = false;
  public isShowingResult: boolean = true;
  public finalRes: finalResult;
  public audio: string;

  //radar
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartLabels: Label[] = ["acousticness", "danceability", "energy", "liveness", "speechiness", "valence"];

  public radarChartData: ChartDataSets[] = [
    { data: [0, 1], label: 'Cluster Average Vector' },
    { data: [0, 1], label: 'Your Song' }
  ];
  public radarChartType: ChartType = 'radar';
  radarChartColor: Color[] = [
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    }
  ];

  //bar vector for Classification
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = ["Acousticness", "Danceability", "Energy", "Liveness", "Speechiness", "Valence"];

  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [0, 1], label: 'Class Avgerage Vector' },
    { data: [0, 1], label: 'Your Song' }
  ];
  barChartColors: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    },
    {

      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    }
  ];

  //bat vector of Clustering
  public barChartOptions1: ChartOptions = {
    responsive: true,
  };
  public barChartLabels1: Label[] = ["Danceability", "Energy", "Speechiness", "Acousticness", "Liveness", "Valence", "Tempo"]
    ;
  public barChartType1: ChartType = 'bar';
  public barChartLegend1 = true;
  public barChartPlugins1 = [];

  public barChartData1: ChartDataSets[] = [
    { data: [0, 1], label: 'Clustering Vector' }

  ];
  barChartColors1: Color[] = [
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'black',
      borderWidth: 2
    }
  ];



  /////////////////////////////////////////////TMF////////////////////////////////////////////////////////////////


  //acousticness f
  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Acousticness' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },
  ];


  lineChartLabels: Label[] = this.getMonth();

  lineChartOptions = {
    responsive: true,
    // scales : {
    //   yAxes: [{
    //      ticks: {
    //         steps : 10,
    //         stepValue : 0.1,
    //         max : 1,
    //         min: 0
    //       }
    //   }]
    // }
  };

  lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  //danceability f
  lineChartData1: ChartDataSets[] = [
    { data: [], label: 'Danceability' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },
  ];

  lineChartLabels1: Label[] = this.getMonth();

  lineChartOptions1 = {
    responsive: true
  };

  lineChartColors1: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend1 = true;
  lineChartPlugins1 = [];
  lineChartType1 = 'line';

  //energy f
  lineChartData2: ChartDataSets[] = [
    { data: [], label: 'Energy' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels2: Label[] = this.getMonth();

  lineChartOptions2 = {
    responsive: true
  };

  lineChartColors2: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend2 = true;
  lineChartPlugins2 = [];
  lineChartType2 = 'line';

  //liveness f
  lineChartData3: ChartDataSets[] = [
    { data: [], label: 'Liveness' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels3: Label[] = this.getMonth();

  lineChartOptions3 = {
    responsive: true
  };

  lineChartColors3: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend3 = true;
  lineChartPlugins3 = [];
  lineChartType3 = 'line';

  //loudness f
  lineChartData4: ChartDataSets[] = [
    { data: [], label: 'Loudness' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels4: Label[] = this.getMonth();

  lineChartOptions4 = {
    responsive: true
  };

  lineChartColors4: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend4 = true;
  lineChartPlugins4 = [];
  lineChartType4 = 'line';

  //mode f
  lineChartData5: ChartDataSets[] = [
    { data: [], label: 'Mode' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels5: Label[] = this.getMonth();

  lineChartOptions5 = {
    responsive: true
  };

  lineChartColors5: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey'
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend5 = true;
  lineChartPlugins5 = [];
  lineChartType5 = 'line';

  //speechiness f
  lineChartData6: ChartDataSets[] = [
    { data: [], label: 'Speechiness' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels6: Label[] = this.getMonth();

  lineChartOptions6 = {
    responsive: true
  };

  lineChartColors6: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend6 = true;
  lineChartPlugins6 = [];
  lineChartType6 = 'line';


  //tempo f
  lineChartData7: ChartDataSets[] = [
    { data: [], label: 'Tempo' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels7: Label[] = this.getMonth();

  lineChartOptions7 = {
    responsive: true,
  };

  lineChartColors7: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend7 = true;
  lineChartPlugins7 = [];
  lineChartType7 = 'line';


  //valence f
  lineChartData8: ChartDataSets[] = [
    { data: [], label: 'Valence' },
    { data: [], label: 'Your Song', pointRadius: 0, fill: false },

  ];

  lineChartLabels8: Label[] = this.getMonth();

  lineChartOptions8 = {
    responsive: true
  };

  lineChartColors8: Color[] = [
    {
      backgroundColor: 'rgba(181,221,155, 0.5)',
      borderColor: 'grey',
    },
    {
      backgroundColor: 'rgba(153,153,153, 0.5)',
      borderColor: 'BLACK'
    }
  ]

  lineChartLegend8 = true;
  lineChartPlugins8 = [];
  lineChartType8 = 'line';


  /////////////////////////////////songs table///////////////////////////////////////////////


  title: string = "";
  _id: string = "";
  duration_ms: number = 0;
  duration: string = "";
  track_number: number = 0;
  albumName: string = "sasi";
  clusterNum: string = "0";
  clusterInsights: string[] = ["hjfh", "ncvb"];
  genre: string = "Rock";
  classificationInsights: classificationInsights;
  classificationInsightsArr: string[] = [];
  errors: number[] = [0, 1, 2];
  timeSeriesInsight: string = "vfd";
  image: string = "https: //i.scdn.co/image/552c454001ba87ea580c1f084482991502562996";
  numOfLikes: number = 1057;
  release_date: string;
  similar_songs: track[] = [];
  embedUrl: string = "";
  sanitizer: DomSanitizer;
  searching: boolean = false;



  @ViewChild(dataSource)
  @Output() ShowInsights = new EventEmitter<any>();

  public DiffrentSong: boolean = false;
  public isDetailsShow: boolean = false;

  constructor(private service: SearchService, public http: HttpClient) { }

  absDifference = (vcArr, afArr, featuresArr) => {
    const res = [];
    for (let i = 0; i < vcArr.length; i++) {
      const el = Math.abs((vcArr[i] || 0) - (afArr[i] || 0));
      res[i] = el;
    };


    var dict_res = []
    for (let i = 0; i < featuresArr.length; i++) {
      dict_res[i] = {
        'feature': featuresArr[i],
        'dist': res[i]
      }
    }

    dict_res.sort((a, b) => a.dist > b.dist ? 1 : ((a.dist < b.dist) ? -1 : 0))
    return dict_res.slice(0, 3);


  };

  createFeatArr(val) {
    const res = []
    for (let i = 0; i < 28; i++) {
      res[i] = val;
    }
    return res;
  }


  async onSearch(val) {
    if (val) {
      this.searching = true;
      const apiQuery = {
        'track': val.songName,
        'artist': val.artistName
      };
      this.finalRes = await this.service.produceMessage(apiQuery);
      this.searching = false;
      console.log(this.finalRes)

      this.isThereRes = true;

      this.barChartData[0].data = this.finalRes.insight.classification_res.vectorClassification.toArray();
      this.barChartData[1].data = this.finalRes.track.audio_features.toArray();

      // var af_class_dist_vector = this.absDifference(
      //   this.finalRes.insight.classification_res.vectorClassification.toArray(),
      //   this.finalRes.track.audio_features.toArray(),
      //   ['acousticness','danceability','energy','instrumentalness', 'loudness','speechiness','valence']);

      this.classificationInsights = this.finalRes.insight.classification_res.insights;

      this.classificationInsightsArr = this.absDifference(
        this.finalRes.insight.classification_res.vectorClassification.toArray(),
        this.finalRes.track.audio_features.toArray(),
        ['acousticness', 'danceability', 'energy', 'instrumentalness', 'loudness', 'speechiness', 'valence'])
        .map(v => this.classificationInsights[v['feature']]);

      //af_class_dist_vector.map( v => this.classificationInsights[v])

      this.radarChartData[1].data = this.finalRes.track.audio_features.toArray();
      this.radarChartData[0].data = this.finalRes.insight.clustering_res.vector.toArray();

      this.release_date = this.finalRes.track.album.release_date;
      this.audio = this.finalRes.track.preview_url;
      this.title = this.finalRes.track.title;
      this._id = this.finalRes.track._id;
      this.track_number = this.finalRes.track.track_number;
      this.duration_ms = this.finalRes.track.duration_ms;

      var minutes = Math.floor(this.duration_ms / 60000);
      var seconds = parseInt(((this.duration_ms % 60000) / 1000).toFixed(0));
      this.duration = `${minutes}:${(seconds < 10 ? "0" : "")}${seconds}`;

      this.albumName = this.finalRes.track.album.name;
      this.clusterNum = this.finalRes.insight.clustering_res.cluster;
      this.clusterInsights = this.finalRes.insight.clustering_res.insights;
      var genreLower = this.finalRes.insight.classification_res.genre;
      this.genre = genreLower.charAt(0).toUpperCase() + genreLower.slice(1);
      this.timeSeriesInsight = this.finalRes.insight.time_series.insight.slice(0, -1);

      debugger;
      this.embedUrl = `https://open.spotify.com/embed/track/${this.finalRes.track._id}`
      console.log(this.embedUrl)

      // acousticness
      this.lineChartData[0].data = this.finalRes.insight.time_series.feature_vectors.acousticness.actual;
      this.lineChartData[1].data = this.createFeatArr(this.finalRes.track.audio_features.acousticness);

      this.lineChartData1[0].data = this.finalRes.insight.time_series.feature_vectors.danceability.actual;
      this.lineChartData1[1].data = this.createFeatArr(this.finalRes.track.audio_features.danceability);

      this.lineChartData2[0].data = this.finalRes.insight.time_series.feature_vectors.energy.actual;
      this.lineChartData2[1].data = this.createFeatArr(this.finalRes.track.audio_features.energy);

      this.lineChartData3[0].data = this.finalRes.insight.time_series.feature_vectors.liveness.actual;
      this.lineChartData3[1].data = this.createFeatArr(this.finalRes.track.audio_features.liveness);

      this.lineChartData4[0].data = this.finalRes.insight.time_series.feature_vectors.loudness.actual;
      this.lineChartData4[1].data = this.createFeatArr(this.finalRes.track.audio_features.loudness);

      this.lineChartData5[0].data = this.finalRes.insight.time_series.feature_vectors.mode.actual;
      this.lineChartData5[1].data = this.createFeatArr(this.finalRes.track.audio_features.mode);

      this.lineChartData6[0].data = this.finalRes.insight.time_series.feature_vectors.speechiness.actual;
      this.lineChartData6[1].data = this.createFeatArr(this.finalRes.track.audio_features.speechiness);

      this.lineChartData7[0].data = this.finalRes.insight.time_series.feature_vectors.tempo.actual;
      this.lineChartData7[1].data = this.createFeatArr(this.finalRes.track.audio_features.tempo);

      this.lineChartData8[0].data = this.finalRes.insight.time_series.feature_vectors.valence.actual;
      this.lineChartData8[1].data = this.createFeatArr(this.finalRes.track.audio_features.valence);


      // for(let i=0;i<this.finalRes.similar_songs.length;i++){
      //   this.similar_songs.push(this.finalRes.similar_songs[i]);
      // }

      this.similar_songs = this.finalRes.similar_songs
      console.log(this.similar_songs);

      this.image = this.finalRes.track.artists[0].images[1].url;
      // this.barChartData1[0].data=this.finalRes.insight.classification_res.vectorClassification.toArray();

    }
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url)
  }

  onStarted(val) {
    this.isShowingResult = val;
    if (this.isShowingResult = true) {
      console.log(this.isShowingResult);
    }
  }


  onDiffrentSong() {
    this.isThereRes = false;
    this.isShowingResult = true;
    this.finalRes = null;
    this.image = ""
  }

  likeIt() {
    this.numOfLikes += 1;
    console.log(this.getMonth());
  }

  getMonth(): string[] {
    var CurrntMonth: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",];
    var month = new Date();
    var thisMonth = month.getMonth()
    var res: string[] = [];
    var thisYear = month.getFullYear();
    var lastYear = thisYear - 1;
    var twoYearsAgo = lastYear - 1;
    for (let i = -24; i < 4; i++) {
      var runningMonth = thisMonth + i
      var temp = (runningMonth + 24 + 1) % 12;
      if (temp == 0) {
        temp = 12;
      }
      if (i < -12) {
        res.push(temp + "." + twoYearsAgo);
      }
      else if (i < 0) {
        res.push(temp + "." + lastYear);
      }
      else {
        res.push(temp + "." + thisYear);
      }
    }

    return res;

  }

}


function dataSource(dataSource: any) {
  throw new Error('Function not implemented.');
}


// const ELEMENT_DATA: songsTable=new songsTable(this.finalRes.);


