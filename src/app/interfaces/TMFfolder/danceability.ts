
export class danceability{
    predictions:number[];
    actual:number[];
    constructor(danceabilityJSON){ 
        this.actual=danceabilityJSON['actual'];
        this.predictions=danceabilityJSON['predictions']; 
    }
 }