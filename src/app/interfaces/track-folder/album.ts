export class album{
    name: string; 
    _id: string;
    release_date:string;
    constructor(albumJSON){
        this.name=albumJSON["title"];
        this._id=albumJSON["_id"];
        this.release_date=albumJSON["release_date"];
    }
}