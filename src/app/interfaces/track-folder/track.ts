import {audio_features} from './audio_features';
import {album} from "./album";
import {artists} from './artists'
export class track{

    title: string;
    _id:string;
    duration_ms:number; 
    preview_url:string;
    track_number: number;
    album:album;
    artists:artists[]; 
    audio_features: audio_features;
    artists_names:string;
    
    constructor(trackJSON){
        this.preview_url= trackJSON["preview_url"];
        this._id=trackJSON["_id"];
        this.album=new album(trackJSON['album']);
        this.audio_features=new audio_features(trackJSON['audio_features']);
        this.duration_ms=trackJSON['duration_ms'];
        this.title=trackJSON['title'];
        this.track_number=trackJSON['track_number'];
        this.artists=[];
        trackJSON['artists'].forEach(artist => {
            this.artists.push(new artists(artist))
        });
    }

}