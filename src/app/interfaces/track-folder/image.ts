export class image{
    height: number; 
    url: string;
    width:number;
    constructor(imagesJSON){
        this.height=imagesJSON["height"];
        this.url=imagesJSON["url"];
        this.width=imagesJSON["width"]
    }
}