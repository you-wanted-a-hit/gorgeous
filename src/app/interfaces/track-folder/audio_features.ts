  
export class audio_features{
    danceability: number; 
    energy: number;
    key: number;
    loudness:number;
    mode: number;
    speechiness:number; 
    acousticness: number; 
    instrumentalness:number; 
    liveness: number;
    valence: number;
    tempo: number;
    duration_ms: number;
    time_signature: number;


    constructor(afJSON){
        this.acousticness=afJSON["acousticness"];
        this.danceability=afJSON["danceability"];
        this.duration_ms=afJSON["duration_ms"];
        this.energy=afJSON["energy"];
        this.instrumentalness=afJSON["instrumentalness"];
        this.key=afJSON["key"];
        this.liveness=afJSON["liveness"];
        this.loudness=afJSON["loudness"];
        this.mode=afJSON["mode"];
        this.speechiness=afJSON["speechiness"];
        this.tempo=afJSON["tempo"];
        this.time_signature=afJSON["time_signature"];
        this.valence=afJSON["valence"];
    }
    toArray(){
        return [this.acousticness,this.danceability,this.energy,this.liveness,this.speechiness,this.valence,0,1];
    }
    toArray1(){
        return [this.acousticness,this.danceability,this.energy,this.instrumentalness,this.loudness,this.speechiness,this.valence,0,1];
    }

}