import { image } from "./image";

export class artists{
    name: string;
    _id:string;
    images:image[];
    constructor(artistsJSON){
        this._id=artistsJSON["_id"];
        this.name=artistsJSON["name"];
        this.images=artistsJSON["images"]
        
    }
}