export class vectorClassification{
    danceability:number;
    energy: number;
    loudness: number;
    speechiness: number;
    acousticness:number;
    instrumentalness:number;
    valence:number;
    constructor(vectorJSON){
        this.acousticness=vectorJSON['acousticness'];
        this.danceability=vectorJSON['danceability'];
        this.energy=vectorJSON['energy'];
        this.instrumentalness=vectorJSON['instrumentalness'];
        this.loudness=vectorJSON['loudness'];
        this.speechiness=vectorJSON['speechiness'];
        this.valence=vectorJSON['valence'];
    }
    toArray(){
        return [this.acousticness,this.danceability,this.energy,this.instrumentalness,this.loudness,this.speechiness,this.valence,0,1];
    }
    
}


