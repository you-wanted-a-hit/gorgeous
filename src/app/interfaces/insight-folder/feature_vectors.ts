import {danceability} from '../TMFfolder/danceability';
import {energy} from '../TMFfolder/energy';
import {loudness} from '../TMFfolder/loudness';
import {mode} from '../TMFfolder/mode';
import {speechiness} from '../TMFfolder/speechiness';
import {acousticness} from '../TMFfolder/acousticness';
import {liveness} from '../TMFfolder/liveness';
import {valence} from '../TMFfolder/valence';
import {tempo} from '../TMFfolder/tempo';

export class feature_vectors{
    danceability: danceability;
    energy: energy;
    loudness: loudness;
    mode: mode;
    speechiness: speechiness;
    acousticness: acousticness;
    liveness: liveness;
    valence: valence;
    tempo: tempo;
    constructor(feature_vectorsJSON){
        this.acousticness=feature_vectorsJSON['acousticness'];
        this.danceability=feature_vectorsJSON['danceability'];
        this.energy=feature_vectorsJSON['energy'];
        this.loudness=feature_vectorsJSON['loudness'];
        this.mode=feature_vectorsJSON['mode'];
        this.speechiness=feature_vectorsJSON['speechiness'];
        this.acousticness=feature_vectorsJSON['acousticness'];
        this.valence=feature_vectorsJSON['valence'];
        this.tempo=feature_vectorsJSON['tempo'];
      
    }

 }