export class vector{
    danceability: number;
    energy:number;
    speechiness :number;
    acousticness: number;
    liveness:number;
    valence:number;
    tempo: number;
     constructor(vectorJSON){
         this.liveness=vectorJSON['liveness'];
         this.tempo=vectorJSON['tempo'];
         this.acousticness=vectorJSON['acousticness'];
         this.danceability=vectorJSON['danceability'];
         this.energy=vectorJSON['energy'];
         this.speechiness=vectorJSON['speechiness'];
         this.valence=vectorJSON['valence'];
     }
     toArray(){
        return [this.acousticness,this.danceability,this.energy,this.liveness,this.speechiness,this.valence,0,1];
    }

    
}