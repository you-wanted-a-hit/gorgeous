import {vectorClassification} from './vectorClassification';
import {classificationInsights} from './classificationInsights'

export class classification{
    genre :string;
    vectorClassification:vectorClassification; 
    insights: classificationInsights;
    constructor(classificationJSON){
        this.genre=classificationJSON['genre'];
        this.vectorClassification=new vectorClassification(classificationJSON['vector']);
        this.insights=classificationJSON['insights'];
    }

}