import{feature_vectors} from './feature_vectors';

export class time_series{
    errors: number[];
    insight: string;
    feature_vectors:feature_vectors;

    constructor(tsJSON){
        this.errors=tsJSON['errors'];
        this.insight=tsJSON['insight'];
        this.feature_vectors=tsJSON['feature_vectors'];
    }

 }