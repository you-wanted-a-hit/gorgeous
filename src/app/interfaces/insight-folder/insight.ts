
import { clustering } from "./clustering";
import { time_series } from "./time_series";
import { classification } from "./classification";
import {acousticness} from "../TMFfolder/acousticness";
import {danceability} from "../TMFfolder/danceability";
import {energy} from "../TMFfolder/energy";
import {liveness} from "../TMFfolder/liveness";
import {loudness} from "../TMFfolder/loudness";
import {mode} from "../TMFfolder/mode";
import {speechiness} from "../TMFfolder/speechiness";
import {tempo} from "../TMFfolder/tempo";
import {valence} from "../TMFfolder/valence";


export class insight{
    clustering_res:clustering ;
    classification_res: classification;
    time_series:time_series;

    
    constructor(insightJSON){
        this.classification_res=new classification(insightJSON["classification"]);
        this.clustering_res=new clustering(insightJSON["clustering"]);
        this.time_series=new time_series(insightJSON["time_series"]);
    }

}