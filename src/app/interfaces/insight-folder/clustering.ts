import { vector } from "./vector";

export class clustering{
    cluster: string;
    vector:vector;
    insights:string[];
    constructor(clusteringJSON){
        this.cluster=clusteringJSON['cluster'];
        this.insights=clusteringJSON['insights'];
        this.vector=new vector(clusteringJSON['vector']);
    }

}