export class classificationInsights{
    danceability:string;
    energy: string;
    loudness: string;
    speechiness: string;
    acousticness:string;
    instrumentalness:string;
    valence:string;
    constructor(insightJson){
        this.acousticness=insightJson['acousticness'];
        this.danceability=insightJson['danceability'];
        this.energy=insightJson['energy'];
        this.instrumentalness=insightJson['instrumentalness'];
        this.loudness=insightJson['loudness'];
        this.speechiness=insightJson['speechiness'];
        this.valence=insightJson['valence'];
    }
}


