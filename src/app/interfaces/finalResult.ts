import { insight } from "./insight-folder/insight";
import { track } from "./track-folder/track";

 export class finalResult
 {
        insight:insight;
        track:track;
        similar_songs:track[];
        constructor(resJSON){
                this.insight=new insight(resJSON['insight'])
                this.track=new track(resJSON['track'])
                
                this.similar_songs = []
                resJSON['similar_songs'].forEach(element => {
                        this.similar_songs.push(new track(element))
                });
        }
}

