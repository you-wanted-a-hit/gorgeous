
export class song{

    SongName:string;
    ArtistName:string;
    AudioLink:string;
    
    constructor(songJSON){
        this.AudioLink= songJSON["preview_url"];
        this.SongName=songJSON["title"];
        this.ArtistName=songJSON['name'];
        
    }
}
