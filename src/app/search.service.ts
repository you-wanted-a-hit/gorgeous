import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalResult } from './interfaces/finalResult';
// import 'rxjs/add/operator/toPromise';

interface ConsumerInfo {
  instance_id: string,
  base_uri: string
}

interface Message {
  topic: string,
  key: string,
  value: {
    songTitle: string,
    artistName: string
  },
  partition: number,
  offset: number,
}

@Injectable()
export class SearchService {
  public content : any[];
  private consumerName: string;
  private consumerGroup: string;
  private baseURL: string;
  public finalRes: finalResult;
  private headers: {};


  constructor(private http: HttpClient) { 
    this.baseURL = "http://34.71.175.116:5000";
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async produceMessage(value): Promise<finalResult>{
    const headers = {
      'Content-Type': 'application/json'
    }
    var url = `${this.baseURL}/very-secret-proxy`

    let frPromise:Promise<finalResult> = new Promise((resolve, reject) => {
      this.http.post(url, value, { headers: headers, observe:'response'})
        .toPromise()
        .then(res => {
          console.log(res.body);
          this.finalRes = new finalResult(res.body['response']);
          resolve(this.finalRes);
        })
    });

    return frPromise
  }

}
