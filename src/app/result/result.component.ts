import { Component, Input, OnInit, Output } from '@angular/core';
import result from '../../assets/result.json';
import * as clustering_res from '../../assets/clustering_res.json';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {JsonToArrService} from '../json-to-arr.service';
import { finalResult } from '../interfaces/finalResult';
import { clustering } from '../interfaces/insight-folder/clustering';
import { time_series } from '../interfaces/insight-folder/time_series';
import { insight } from '../interfaces/insight-folder/insight';
import { classification } from '../interfaces/insight-folder/classification';
import { vector } from '../interfaces/insight-folder/vector';
import { vectorClassification } from '../interfaces/insight-folder/vectorClassification';
import * as CanvasJs from 'canvasjs'
import { Color, Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent   implements OnInit 
{
  public fs: finalResult;
  public insight:insight;
  public clustering_res:clustering;
  public classification_res:classification;
  public time_series:time_series;
  public vector:vector;
  public vectorClassification:vectorClassification ;
  @Input() finalResult: finalResult; 

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = [ "danceability","energy","speechiness" ,"acousticness","liveness","valence","tempo"]
  ;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [0,1], label: 'Clustering Vector' }
  ];
    barChartColors: Color[] = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(201, 203, 207, 0.2)'
      ],
      borderColor: [
        'black',
        'black',
        'black',
        'black',
        'black',
        'black',
        'black'
      ],
      borderWidth:1
    },
  ];

  constructor(private service: JsonToArrService,public http:HttpClient){
    console.log(this.finalResult);
  }
  [x: string]: any;
  afterRES(val){

  }
  ngOnInit():void {
  
    // this.service.getSongsElement().subscribe((res: SongElement[]) => {
    //   this.dataSource=new MatTableDataSource(res);
    //   console.log('here is the song result'+ this.songsResul );
    // });
  }
  displayedColumns: string[] = ['spotify_id','Position','Artist','Track_Name','Streams','instrumentalness','energy','loudness','speechiness','acousticness','liveness','valence','tempo','cluster','dist'];
  public dataSource =this.resultFromDB;
  
//  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.http.get<finalResult>('https://rest-proxy:8082').subscribe(data => {
      this.finalResult = data;
    })
    // this.vector=this.finalResult.insight.clustering_res.vector;
    // this.vector.danceability=this.finalResult.insight.clustering_res.vector.danceability;
    // this.vector.acousticness=this.finalResult.insight.clustering_res.vector.acousticness;

    // this.vectorClassification=this.finalResult.insight.classification_res.vectorClassification;
    // this.clustering_res=this.finalResult.insight.clustering_res;
    // this.classification_res=this.finalResult.insight.classification_res;
    // this.time_series=this.finalResult.insight.time_series;
    // this.insight=this.finalResult.insight;
    // this.dataSource =this.clustering_res;
    // this.dataSource.paginator = this.paginator;
  }
}


// export interface SongElement {
//   spotify_id: any;
//   Position: any;
//   Artist: any;
//   Track_Name: any;
//   Streams:any;
//   instrumentalness:any;
//   energy:any;
//   loudness:any;
//   speechiness:any;
//   acousticness:any;
//   liveness:any;
//   valence:any;
//   tempo:any;
//   cluster:any;
//   dist:any;

// }

  // @ViewChild('dataTable') dataTable: MatTable<any>;
  // dataSource: MatTableDataSource<SongElement[]> ;
  
  // ngOnInit() {
  //     let dataSamples: SongElement[];
  //     // dataSamples= clustering_res  as SongElement[];
  //     console.log('here is our data'+dataSamples);
  //     //init your list with ItemModel Objects (can be manual or come from server etc) And put it in data source
  //     this.dataSource = new MatTableDataSource<SongElement[]>(this.dataSamples);
  //     if(this.dataSource){
  //           this.dataTable.renderRows();
  //     }
  // }
 


