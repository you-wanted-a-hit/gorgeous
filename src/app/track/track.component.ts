import { Component, Input, OnInit, Output } from '@angular/core';
import result from '../../assets/result.json';
import * as clustering_res from '../../assets/clustering_res.json';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import { from, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {JsonToArrService} from '../json-to-arr.service';
import {finalResult} from '../interfaces/finalResult';
import {classification} from '../interfaces/insight-folder/classification';
import { Color, Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType, RadialChartOptions } from 'chart.js';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {
  public resultFromDB:classification;
  public classification:classification;
  // public finalResult: finalResult;
  @Input() finalResult: finalResult; 
  //features radar
  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartLabels: Label[] = ["danceability","energy","mode","speechiness","acousticness", "instrumentalness","liveness","valence"]

  public radarChartData: ChartDataSets[] = [
    { data: [0,1], label: 'Employee Skill Analysis' }
  ];
  public radarChartType: ChartType = 'radar';


//bar vector
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = [ "danceability","energy","speechiness" ,"acousticness","liveness","valence","tempo"]
  ;
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [0.1, 0.9, 0.5, 0.6, 0.5, 0.2,0.85,0,1], label: 'Clustering Vector' }
  ];
    barChartColors: Color[] = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(201, 203, 207, 0.2)'
      ],
      borderColor: [
        'black',
        'black',
        'black',
        'black',
        'black',
        'black',
        'black'
      ],
      borderWidth:1
    },
  ];


  constructor(private service: JsonToArrService,public http:HttpClient){}
  
  [x: string]: any;
  ngOnInit(): void { 
    // this.service.getSongsElement().subscribe((res: SongElement[]) => {
    //   this.dataSource=new MatTableDataSource(res);
    //   console.log('here is the song result'+ this.songsResul );
    // });
  }
  displayedColumns: string[] = ['name','artist','spotify_id','danceability','energy','key','loudness','mode','speechiness','acousticness'
  ,'instrumentalness','liveness','valence','tempo','duration_ms','time_signature','genre'];
  public dataSource:classification;
  
//  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.http.get<finalResult>('https://rest-proxy:8082').subscribe(data => {
      this.finalResult = data;
    })
    this.classification_res=this.finalResult.insight.classification_res
    this.dataSource =this.classification_res;
    //this.dataSource.paginator = this.paginator;
  }
}



  