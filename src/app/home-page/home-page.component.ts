import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  @Output() public started = new EventEmitter<any>();
  public goToSearchComp: boolean;
  

  constructor() { }


  ngOnInit(): void {

  }

  onStarted()
  {
    
    this.goToSearchComp=true;
    console.log(this.goToSearchComp);
    this.started.emit(this.goToSearchComp);
  }

}
