### STAGE 1: Build ###
FROM node:16.2-buster-slim AS build

WORKDIR /usr/src/app

COPY package.json ./

RUN apt update 
RUN apt install git -y 
RUN npm install --only=prod --force
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular

COPY . .

RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY --from=build /usr/src/app/dist/gorgeous /usr/share/nginx/html